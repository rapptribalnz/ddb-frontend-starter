# DDB Front-end Starter

[![Issues](https://img.shields.io/bitbucket/issues-raw/rapptribalnz/ddb-frontend-starter.svg)](https://bitbucket.org/rapptribalnz/ddb-frontend-starter/issues)
[![Build](https://img.shields.io/bitbucket/pipelines/rapptribalnz/ddb-frontend-starter/master.svg)](https://bitbucket.org/rapptribalnz/ddb-frontend-starter/addon/pipelines/home)

## Introduction

DDB Front-end Starter is created for the DDB New Zealand digital team to get started quickly with common build tools for projects. It does not enforce directory structure, nor libraries.

This starter is to be used as a base for all types of projects. VueJS, React, SilverStripe or Umbraco.

## Documentation

Most of the work here is just an opinionated configuration of Laravel-Mix. For documentation visit https://laravel-mix.com/docs/4.0/

## Getting started

The file `webpack.mix.js` is the build configuration file where all the options to build your application lives.

We also have a few dot files floating around.

- `.browserslistrc` - This is a specification for browser support. This will give our build tool instructions for CSS and JS browser support.
- `.editorconfig` - This is a set of criteria for your IDE to adhear to so that all our developers are using the same indentation rules etc.
- `.eslintrc` - This is a linting specification to keep us honest and make us better developers.

## Performance

To keep your website performance within budget - use Lighthouse. https://web.dev/use-lighthouse-for-performance-budgets

The initial `lighthouserc.json` file is included.

#### Test it out locally in a Docker container

The only tricky thing here is mounting the container in such a way that it has access to the files we've created in our current working dir. This command will map our directory to /tmp in the container.

`docker run -it --volume $(pwd):/tmp node:10 /bin/bash`

Then once you're in the container run the following to run the script. You should see it run through the installation and print a result to stdout.

`cd /tmp && bash lighthouse-checks $YOUR_URL_HERE`


## Commands

We have the usual commands here.

During development: `yarn watch`
This will open up a browser window to https://localhost:3000/ To disable this, comment out `browserSync` in `webpack.mix.js`.

To generate a production build: `yarn production`

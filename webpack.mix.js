const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

// Laravel-Mix Options
const mixOptions = {
  terser: {
    parallel: true, // Use multi-threading for the processing
    terserOptions: {
      mangle: mix.inProduction(),
      warnings: mix.inProduction(),
      compress: mix.inProduction(),
    },
  },
  processCssUrls: false,
  postCss: [ tailwindcss('./tailwind.config.js') ],
  purifyCss: false,
  clearConsole: mix.inProduction(),
  cssNano: {
    preset: 'default',
  },
};

if (mix.inProduction()) {
  // https://github.com/webpack-contrib/purifycss-webpack#options
  mixOptions.purifyCss = {
    paths: ['./public/*'],
  };
}

mix
  // Laravel-Mix Options
  .options(mixOptions)

  // Stylesheets
  .sass('src/sass/app.scss', 'public/css')

  // JavaScript
  .js('src/js/app.js', 'public/js')
  .sourceMaps()
  // .react('src/js/app.jsx', 'public/js/app.js') // Example of React support

  // Extract the JavaScript libraries that rarely change.
  // This generates a vendor.js + manifest.js file.
  // https://laravel-mix.com/docs/5.0/extract
  // .extract(['jquery', 'bootstrap', 'axios'])

  // Enable browser-sync for your development domain.  To use, run `yarn watch`.
  // .browserSync('my-domain.test')
  .browserSync({
    server: 'public',
    https: true,
    proxy: false,
  })

  // If you need to copy files or directories into the dist/public directory.
  // https://laravel-mix.com/docs/5.0/copying-files
  .copy(['src/index.html', 'src/favicon.ico'], 'public/')
  .copyDirectory('src/fonts', 'public/fonts')
  .copyDirectory('src/images', 'public/images')

  // Native Webpack config. Extends the default Laravel-Mix Webpack config.
  .webpackConfig({
    resolve: {
      modules: [
        'node_modules',
      ],
      alias: {},
    },
  });

// if (mix.inProduction()) {
//   mix.version();
// }
